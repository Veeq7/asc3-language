# ASC3 Language

Adds support for Starcraft AI ASC3 language.

## Suggestions

For best experience with the addon please consider:
- Using `"editor.snippetSuggestions": "top"` setting.
- Using `Monokai` theme.

## Resources

[AI Starter Guide](http://pr0nogo.wikidot.com/rs-ai-starter)<br>
[AI Command Guide](http://pr0nogo.wikidot.com/rs-ai)<br>
[Extended AI Command Guide](http://pr0nogo.wikidot.com/rs-ai-starter)

## Tools

[Development Toolkit](https://gitlab.com/Veeq7/PyMS-Veeq)<br>
[Preprocessor](https://gitlab.com/Veeq7/asc3-preprocessor)